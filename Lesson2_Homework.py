"""
1.Task
Form a string that contains information about a particular word.
"Word [word here] has [here the word length, get from the most words] letters",
for example "Word 'Python' has 6 letters".
To get a word for analysis, use the constant or the input() function.
"""

# 1 Task
print("1 Task")
print("Word length exercise")
word = input("Enter a word: ").strip()  # in case you accidentally put 'space' before or after your word

check_for_mistakes = word.isalpha()  # in case you accidentally put digits or spaces inside your word
if check_for_mistakes:
    print(f"Word '{word}' has {len(word)} letters")
else:
    print("Input incorrect.")   # this message will be print if you have letters or "spaces" inside you input
print()

"""
2.Task
Write the program "Cashier in the movie", which asks the user to enter their own age 
(you can use a constant or input() function, only one message should be displayed on the screen, 
and also think about options when incorrect data is entered).
if the user is less than 7 - display the message"Where are your parents?"
if the user is less than 16 - display the message "This is an adult movie!"
if the user is more than 65 - display the message"Show the pension certificate!"
if the user's age contains the number 7 - display the message "You will be lucky today!"
in any other case - display the message "And there are no more tickets!"
"""

# 2 Task
print("2 Task")
print("Cashier in the movie")

user_age = input("Enter your age: ").strip()  # in case you accidentally put 'space' before or after your input

check_for_mistakes = user_age.isdigit()
if check_for_mistakes:  # check for correct input
    user_age_int = int(user_age)
    if user_age_int < 7:  # check for user age
        print("Where is your parents?")
    elif user_age_int < 16:
        print("This is adult movie!")
    elif user_age_int > 65:
        print("Show the pension certificate!")
    elif '7' in user_age:
        print("You will be lucky today!")
    elif user_age_int <= 65:
        print("And there are no more tickets!")

else:
    print("Incorrect input")  # this message will be print if you have letters or "spaces" inside you input
